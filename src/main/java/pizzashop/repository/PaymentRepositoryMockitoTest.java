package pizzashop.repository;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class PaymentRepositoryMockitoTest {

    private PaymentRepository repo = new PaymentRepository();
    @Test
    void add() {
        PaymentRepository spiedRepo = spy(repo);
        spiedRepo.deleteAll();
        Payment payment= new Payment(2, PaymentType.Cash,15.5);
        try {
            spiedRepo.add(payment);
            assertEquals(spiedRepo.getAll().size(),1);
            verify(spiedRepo,times(1)).add(payment);

            ValidatorException exception = assertThrows(ValidatorException.class, () -> spiedRepo.add(new Payment(-4,PaymentType.Cash,20)));
            assertEquals("Invalid table number!\n", exception.getMessage());
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
    }

    @Test
    void getAll() {
        PaymentRepository spiedRepo = spy(repo);
        spiedRepo.deleteAll();
        assertEquals(spiedRepo.getAll().size(),0);
        try {
            Payment payment1 = new Payment(2,PaymentType.Cash,14);
            Payment payment2 = new Payment(3,PaymentType.Card,24);
            Payment payment3 = new Payment(4,PaymentType.Cash,34);
            spiedRepo.add(payment1);
            verify(spiedRepo,times(1)).add(payment1);
            spiedRepo.add(payment2);
            verify(spiedRepo,times(1)).add(payment2);
            spiedRepo.add(payment3);
            verify(spiedRepo,times(1)).add(payment3);
            List<Payment> paymentList = spiedRepo.getAll();
            assertEquals(paymentList.size(),3);
            assertEquals(paymentList.get(0),payment1);
            assertEquals(paymentList.get(1),payment2);
            assertEquals(paymentList.get(2),payment3);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
    }
}