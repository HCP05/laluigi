package pizzashop.repository;

import pizzashop.model.Payment;

public class PaymentValidator {
    void validate(Payment paymentToValidate) throws ValidatorException {
        String error="";
        if(paymentToValidate.getTableNumber() < 1 || paymentToValidate.getTableNumber() > 8)
            error+="Invalid table number!\n";
        if(paymentToValidate.getType() == null)
            error+="Invalid payment type!\n";
        if(paymentToValidate.getAmount() <= 0)
            error+="Invalid payment amount!\n";

        if(error.length()>0)
            throw new ValidatorException(error);
    }

}
