package pizzashop.repository;

import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class PaymentRepository {
//    private static String filename = "data/payments.txt";
    private static String filename = "C:\\Users\\Oana Ruxandra Pop\\Desktop\\VVSS\\Git\\laluigi\\src\\main\\resources\\data\\payments.txt";

    //    private static String filename = "C:\\Users\\papy_\\Desktop\\An3Sem2\\VVSS\\main\\03_PizzaShop\\src\\main\\resources\\data\\payments.txt";
    private List<Payment> paymentList;

    private PaymentValidator paymentValidator;
    public PaymentRepository(){
        this.paymentList = new ArrayList<>();
        this.paymentValidator = new PaymentValidator();
        readPayments();
    }

    public PaymentRepository(List<Payment> arrayList){
        this.paymentList = arrayList;
        this.paymentValidator = new PaymentValidator();
    }
    public PaymentRepository(String fileName){
        this.paymentList = new ArrayList<>();
        this.paymentValidator = new PaymentValidator();
        this.filename = fileName;
        readPayments();
    }
    private void readPayments(){
        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
//        File file = new File(classLoader.getResource(filename).getFile());
        File file = new File(filename);
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = null;
            while((line=br.readLine())!=null){
                Payment payment=getPayment(line);
                paymentList.add(payment);
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Payment getPayment(String line){
        Payment item=null;
        if (line==null|| line.equals("")) return null;
        StringTokenizer st=new StringTokenizer(line, ",");
        int tableNumber= Integer.parseInt(st.nextToken());
        String type= st.nextToken();
        double amount = Double.parseDouble(st.nextToken());
        item = new Payment(tableNumber, PaymentType.valueOf(type), amount);
        return item;
    }

    public void add(Payment payment) throws ValidatorException {
        paymentValidator.validate(payment);
        paymentList.add(payment);
        writeAll();
    }

    public List<Payment> getAll(){
        return paymentList;
    }

    public void deleteAll() { paymentList = new ArrayList<>();}

    public void writeAll(){
        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
//        File file = new File(classLoader.getResource(filename).getFile());
        File file = new File(filename);

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(file));
            for (Payment p:paymentList) {
                System.out.println(p.toString());
                bw.write(p.toString());
                bw.newLine();
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}