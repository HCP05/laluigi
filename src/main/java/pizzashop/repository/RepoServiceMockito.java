package pizzashop.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.PaymentType;
import pizzashop.service.PizzaService;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class RepoServiceMockito {
    private PizzaService pizzaService;
    private PaymentRepository paymentRepository;

    @BeforeEach
    void setUp() {
        MenuRepository menuRepository = mock(MenuRepository.class);

        paymentRepository = new PaymentRepository(new ArrayList<>());
        pizzaService = new PizzaService(menuRepository, paymentRepository);
    }

    @Test
    void addPayment1() {
        int tableNumber = 3;
        PaymentType type = PaymentType.Cash;
        double amount = 5.0;

        try{
            pizzaService.addPayment(tableNumber, type, amount);
            assertTrue(true);
        }catch (Exception e){
            assertTrue(false);
        }

        assertEquals(1, paymentRepository.getAll().size());
    }

    @Test
    void addPayment2() {
        int tableNumber = -1;
        PaymentType type = PaymentType.Cash;
        double amount = 5.0;

        try {
            pizzaService.addPayment(tableNumber, type, amount);
            assertTrue(false);
        }catch (
                Exception e
        ){
            assertTrue(true);
        }
        assertEquals(0, paymentRepository.getAll().size());
    }
}
