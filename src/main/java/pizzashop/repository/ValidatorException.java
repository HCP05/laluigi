package pizzashop.repository;

public class ValidatorException extends Exception {
    String message;

    ValidatorException(String message) {
        this.message = message;
    }

    public String getMessage(){
        return this.message;
    }
}
