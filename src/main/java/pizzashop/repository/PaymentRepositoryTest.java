package pizzashop.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PaymentRepositoryTest {

    private PaymentRepository payRepo = new PaymentRepository("data/paymentsTest.txt");

    @BeforeEach
    void setUp() {
        payRepo.deleteAll();
    }
    @Nested
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    class validEc{
        @Test
        @DisplayName("1st valid EC")
        @Tag("valid")
        @Order(1)
        void TC1_ECP() throws ValidatorException {
            payRepo.add(new Payment(5,PaymentType.Cash,500));
            List<Payment> paymentList = payRepo.getAll();
            Payment addedPayment = paymentList.get(0);
            assertEquals(addedPayment.getAmount(),500);
            assertEquals(addedPayment.getTableNumber(),5);
            assertEquals(addedPayment.getType(),PaymentType.Cash);
        }
        @Test
        @DisplayName("2nd valid EC")
        @Tag("valid")
        @Order(2)
        void TC2_ECP() throws ValidatorException {
            payRepo.add(new Payment(1,PaymentType.Card,10));
            List<Payment> paymentList = payRepo.getAll();
            Payment addedPayment = paymentList.get(0);
            assertEquals(addedPayment.getAmount(),10);
            assertEquals(addedPayment.getTableNumber(),1);
            assertEquals(addedPayment.getType(),PaymentType.Card);
        }
        @Test
        @DisplayName("1st BVA valid EC")
        @Tag("valid")
        @Order(5)
        void TC5_BVA() throws ValidatorException{
            payRepo.add(new Payment(1,PaymentType.Card,20));
            List<Payment> paymentList = payRepo.getAll();
            Payment addedPayment = paymentList.get(0);
            assertEquals(addedPayment.getAmount(),20);
            assertEquals(addedPayment.getTableNumber(),1);
            assertEquals(addedPayment.getType(),PaymentType.Card);
        }

        @Test
        @DisplayName("2nd BVA valid EC")
        @Tag("valid")
        @Order(6)
        void TC6_BVA() throws ValidatorException{
            payRepo.add(new Payment(8,PaymentType.Card,20));
            List<Payment> paymentList = payRepo.getAll();
            Payment addedPayment = paymentList.get(0);
            assertEquals(addedPayment.getAmount(),20);
            assertEquals(addedPayment.getTableNumber(),8);
            assertEquals(addedPayment.getType(),PaymentType.Card);
        }
    }


    @Nested
    class nonValidEc{
        @Test
        @DisplayName("1st non valid EC")
        @Tag("non-valid")
        @Order(3)
        void TC3_ECP(){
            ValidatorException exception = assertThrows(ValidatorException.class, () -> payRepo.add(new Payment(0,PaymentType.Card,50)));
            assertEquals("Invalid table number!\n", exception.getMessage());
        }

        @DisplayName("2nd non valid EC")
        @Tag("non-valid")
        @Order(4)
        @ParameterizedTest
        @ValueSource(ints = { -1, -4 })
        void TC4_ECP(int amount){
            ValidatorException exception = assertThrows(ValidatorException.class, () -> payRepo.add(new Payment(2,null,amount)));
            assertEquals("Invalid payment type!\nInvalid payment amount!\n", exception.getMessage());
            ValidatorException exception2 = assertThrows(ValidatorException.class, () -> payRepo.add(new Payment(2,PaymentType.Card,amount)));
            assertEquals("Invalid payment amount!\n", exception2.getMessage());
        }

        @Test
        @DisplayName("1st BVA non-valid EC")
        @Tag("non-valid")
        @Order(7)
        void TC7_BVA() throws ValidatorException{
            ValidatorException exception = assertThrows(ValidatorException.class, ()-> payRepo.add(new Payment(0,PaymentType.Card,20)));
            assertEquals(exception.getMessage(),"Invalid table number!\n");
        }
        @Test
        @DisplayName("1st BVA non-valid EC")
        @Tag("non-valid")
        @Order(8)
        void TC8_BVA() throws ValidatorException{
            ValidatorException exception = assertThrows(ValidatorException.class, ()-> payRepo.add(new Payment(9,PaymentType.Card,20)));
            assertEquals(exception.getMessage(),"Invalid table number!\n");
        }
    }
}