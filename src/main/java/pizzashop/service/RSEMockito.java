package pizzashop.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.repository.ValidatorException;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.Mockito.mock;

public class RSEMockito {
    private PizzaService pizzaService;
    private PaymentRepository paymentRepository;

    @BeforeEach
    void setUp() {
        MenuRepository menuRepository = mock(MenuRepository.class);

        paymentRepository = new PaymentRepository(new ArrayList<>());//IN MEMORY
        pizzaService = new PizzaService(menuRepository, paymentRepository);
    }

    @Test
    void addPayment1() {
        int tableNumber = 3;
        PaymentType type = PaymentType.Cash;
        double amount = 5.0;
        Payment payment = new Payment(tableNumber, type, amount);

        try {
            pizzaService.addPayment(tableNumber, type, amount);
            assertTrue(true);
        } catch (ValidatorException e) {
            assertTrue(false);
        }

        assertEquals(1, paymentRepository.getAll().size());
        assertNotEquals(-1, paymentRepository.getAll().indexOf(payment));
    }

    @Test
    void addPayment2() {
        Payment payment1 = new Payment(1, PaymentType.Cash, 3);
        Payment payment2 = new Payment(2, PaymentType.Card, 8);
        Payment payment3 = new Payment(3, PaymentType.Cash, 12);

        try {
            paymentRepository.add(payment1);
            paymentRepository.add(payment2);
            paymentRepository.add(payment3);
        }catch (Exception e){
            assertTrue(false);
        }

        double result = 0;
        try {
            result = pizzaService.getTotalAmount(PaymentType.Cash);
            assertTrue(true);
        } catch (Exception e) {
            assertTrue(false);
        }

        assertEquals(payment1.getAmount() + payment3.getAmount(), result);
    }
}
