package pizzashop.service;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import static org.junit.jupiter.api.Assertions.*;

class PizzaServiceTest {

    private MenuRepository menuRepo = new MenuRepository();

    private static String filename = "C:\\Users\\Oana Ruxandra Pop\\Desktop\\VVSS\\Git\\laluigi\\src\\main\\resources\\data\\paymentsTest2.txt";

    private PaymentRepository payRepo = new PaymentRepository(filename);
    private PizzaService service = new PizzaService(menuRepo,payRepo);

    @Test
    void TC1() {
        double totalCardAmount = service.getTotalAmount(PaymentType.Card);
        assertEquals(totalCardAmount,105.7);

        double totalCashAmount = service.getTotalAmount(PaymentType.Cash);
        assertEquals(totalCashAmount,56);
    }

    @Test
    void TC2(){
        double totalAmount = service.getTotalAmount(null);
        assertEquals(totalAmount,0);
    }
    @Test
    void TC3(){
        payRepo.deleteAll();
        double totalCardAmount = service.getTotalAmount(PaymentType.Card);
        assertEquals(totalCardAmount,0);

        double totalCashAmount = service.getTotalAmount(PaymentType.Cash);
        assertEquals(totalCashAmount,0);
    }
}