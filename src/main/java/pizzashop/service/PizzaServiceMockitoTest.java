package pizzashop.service;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.repository.ValidatorException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class PizzaServiceMockitoTest {

    @Spy
    private MenuRepository menuRepo;
    @Spy
    private PaymentRepository paymentRepo;
    @InjectMocks
    private PizzaService service;

    @BeforeEach
    public void setUp(){ MockitoAnnotations.initMocks(this); }

    @Test
    void addPayment()  {
        MenuRepository spiedMenuRepo = spy(menuRepo);
        paymentRepo.deleteAll();
        PaymentRepository spiedPaymentRepo = spy(paymentRepo);
        service = new PizzaService(spiedMenuRepo,spiedPaymentRepo);
        PizzaService spiedService = spy(service);

        Payment payment1 = new Payment(2, PaymentType.Card,14);
        try {
            Mockito.doNothing().when(spiedPaymentRepo).add(payment1);
            spiedService.addPayment(payment1);
            assertEquals(spiedService.getPayments().size(),0);

            spiedService.addPayment(new Payment(2,PaymentType.Card,15));
            assertEquals(spiedService.getPayments().size(),1);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }

        ValidatorException exception = assertThrows(ValidatorException.class, ()-> spiedService.addPayment(new Payment(0,PaymentType.Card,20)));
        assertEquals(exception.getMessage(),"Invalid table number!\n");
    }

    @Test
    void getTotalAmount() throws ValidatorException {
        MenuRepository spiedMenuRepo = spy(menuRepo);
        paymentRepo.deleteAll();
        PaymentRepository spiedPaymentRepo = spy(paymentRepo);
        service = new PizzaService(spiedMenuRepo,spiedPaymentRepo);
        PizzaService spiedService = spy(service);

        spiedService.addPayment(new Payment(4,PaymentType.Cash,35));
        spiedService.addPayment(new Payment(3,PaymentType.Cash,41.5));
        spiedService.addPayment(new Payment(7,PaymentType.Card,7.8));

        assertEquals(spiedService.getTotalAmount(PaymentType.Card),7.8);
        assertEquals(spiedService.getTotalAmount(PaymentType.Cash),76.5);
        assertEquals(spiedService.getTotalAmount(null),0);
    }
}