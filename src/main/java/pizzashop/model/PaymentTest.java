package pizzashop.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class PaymentTest {
    private Payment payment;
    private Random random;

    @BeforeEach
    public void setUp() {
        payment = new Payment(1, PaymentType.Card, 1);
        random = new Random(LocalDateTime.now().toEpochSecond(ZoneOffset.ofHours(0)));
    }

    private Payment payment1 = new Payment(5,PaymentType.Card,10);
    private Payment payment2 = new Payment(2,PaymentType.Cash,37.5);
    @Test
    void getTableNumber() {
        assertEquals(payment1.getTableNumber(),5);
        assertEquals(payment2.getTableNumber(),2);
    }

    @Test
    void setTableNumber() {
        payment1.setTableNumber(1);
        payment2.setTableNumber(3);
        assertEquals(payment1.getTableNumber(),1);
        assertEquals(payment2.getTableNumber(),3);
    }

    @Test
    void getType() {
        assertEquals(payment1.getType(),PaymentType.Card);
        assertEquals(payment2.getType(),PaymentType.Cash);
    }

    @Test
    void setType() {
        payment1.setType(PaymentType.Cash);
        payment2.setType(PaymentType.Card);
        assertEquals(payment1.getType(),PaymentType.Cash);
        assertEquals(payment2.getType(),PaymentType.Card);
    }

    @Test
    void getAmount() {
        assertEquals(payment1.getAmount(),10);
        assertEquals(payment2.getAmount(),37.5);
    }

    @Test
    void setAmount() {
        payment1.setAmount(14);
        payment2.setAmount(77.7);
        assertEquals(payment1.getAmount(),14);
        assertEquals(payment2.getAmount(),77.7);
    }
}