package org.example.steps.serenity;

import net.thucydides.core.pages.PageObject;
import org.example.pages.DictionaryPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.junit.Assert;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUserSteps extends PageObject {

    DictionaryPage dictionaryPage;

    @Step
    public void enters(String keyword) {
        dictionaryPage.enter_keywords(keyword);
    }

    @Step
    public void starts_search() {
        dictionaryPage.lookup_terms();
    }

    @Step
    public void should_see_definition(String definition) {
        assertThat(dictionaryPage.getDefinitions(), hasItem(containsString(definition)));
    }

    @Step
    public void is_the_home_page() {
        dictionaryPage.open();
    }

    @Step
    public void looks_for(String term) {
        enters(term);
        starts_search();
    }

    @Step("Enter Name")
    public void inputName(String name) {
       dictionaryPage.inputName.sendKeys((name));
//        waitFor(inputName);
//        typeInto(inputName, name);
    }
    @Step("Enter Name")
    public void inputEmail(String name) {
        dictionaryPage.email.sendKeys((name));
//        waitFor(inputName);
//        typeInto(inputName, name);
    }

    @Step("Enter Name")
    public void inputPhone(String name) {
        dictionaryPage.phone.sendKeys((name));
//        waitFor(inputName);
//        typeInto(inputName, name);
    }
    @Step("Enter Name")
    public void inputSubject(String name) {
        dictionaryPage.subject.sendKeys((name));
//        waitFor(inputName);
//        typeInto(inputName, name);
    }

    @Step("Enter Name")
    public void inputDescription(String name) {
        dictionaryPage.description.sendKeys((name));
//        waitFor(inputName);
//        typeInto(inputName, name);
    }


    @Step("Click Submit Button")
    public void clickSubmit() {
        dictionaryPage.submitButton.click();
    }

    @Step("Test")
    public void verifyPage() {
        assert(dictionaryPage.alert.getText().contains("blan")==true);

    }
    @Step("TestValid")
    public void verifySubmission(){
        assert(dictionaryPage.document.getText().contains("Testaredadada")==true);
    }
}