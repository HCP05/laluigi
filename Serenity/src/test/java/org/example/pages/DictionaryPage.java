package org.example.pages;

import com.google.common.base.Predicates;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.WebElementFacade;

import java.lang.annotation.Documented;
import java.util.stream.Collectors;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.util.List;

@DefaultUrl("https://automationintesting.online/#/")
public class DictionaryPage extends PageObject {

    @FindBy(tagName = "p")
    public WebElementFacade document;
    @FindBy(id="search")
    private WebElementFacade searchTerms;

    @FindBy(id="name")
    public WebElementFacade inputName;

    @FindBy(id = "email")

    public WebElementFacade email;

    @FindBy(id = "phone")
    public WebElementFacade phone;

    @FindBy(id = "subject")
    public WebElementFacade subject;


    @FindBy(id = "description")
    public WebElementFacade description;
    @FindBy(className = "alert-danger")
    public WebElementFacade alert;
    @FindBy(id = "submitContact")
    public WebElementFacade submitButton;

    @FindBy(name="go")
    private WebElementFacade lookupButton;

    public void enter_keywords(String keyword) {
        searchTerms.type(keyword);
    }

    public void lookup_terms() {
        lookupButton.click();
    }

    public List<String> getDefinitions() {
        WebElementFacade definitionList = find(By.tagName("ol"));
        return definitionList.findElements(By.tagName("li")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }



}