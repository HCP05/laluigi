package org.example.features.search;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

import net.thucydides.core.annotations.Title;
import org.example.pages.DictionaryPage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import org.example.steps.serenity.EndUserSteps;

@RunWith(SerenityRunner.class)
public class SearchByKeywordStory {

    @Managed(uniqueSession = true,driver = "firefox")
    public WebDriver webdriver;

    @Steps
    public EndUserSteps anna;

//    @Issue("#WIKI-1")
//    @Test
//    public void searching_by_keyword_apple_should_display_the_corresponding_article() {
//        anna.is_the_home_page();
//        anna.looks_for("apple");
//        anna.should_see_definition("A common, round fruit produced by the tree Malus domestica, cultivated in temperate climates.");
//
//    }
//
//    @Test
//    public void searching_by_keyword_banana_should_display_the_corresponding_article() {
//        anna.is_the_home_page();
//        anna.looks_for("pear");
//        anna.should_see_definition("pear");
//    }
//
//    @Test
//    public void searching_by_ambiguious_keyword_should_display_the_disambiguation_page() {
//    }

    @Test
    @Title("Succesful submit")

    public void successfulSubmit() {

        // When
        anna.is_the_home_page();
        anna.inputName("Admindaawd dawwa");
        anna.inputEmail("test@gmail.com");
        anna.inputPhone("075828272922");
        anna.inputSubject("Testaredadada");
        anna.inputDescription("Descri ADDWAD WQDWQWQWDQW DQWDQWDQWDQere");
        anna.clickSubmit();

        // Then
//        Serenity.reportThat("Passing valid credentials navigates to DashBoard page",
//                () -> assertThat(dashboardPage.getHeading()).isEqualToIgnoringCase("DashBoard"));
    }

    @Test
    @Title("Unsuccessful submit")
    public void unssucess(){
        anna.is_the_home_page();
        anna.clickSubmit();
        anna.verifyPage();

    }
} 